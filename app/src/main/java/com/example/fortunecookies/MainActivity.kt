package com.example.fortunecookies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.ImageView
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var cookieImageView: ImageView
    private lateinit var messageTextView: TextView
    private val messages = arrayOf(
        "Acredite em si mesmo e tudo será possível.",
        "Você é capaz de conquistar seus sonhos.",
        "A persistência é o caminho do sucesso.",
        "Não deixe que o medo impeça você de tentar.",
        "Nunca é tarde demais para começar algo novo.",
        "A felicidade está dentro de você, não nas coisas ao seu redor.",
        "Acredite em si mesmo e tudo será possível.",
        "Nunca desista. Grandes coisas levam tempo.",
        "O fracasso é apenas uma oportunidade para recomeçar com mais inteligência.",
        "O sucesso é uma jornada, não um destino final.",
        "Não deixe o medo de falhar impedir você de tentar.",
        "Tudo é possível se você acreditar e trabalhar duro.",
        "A determinação é a chave para alcançar seus objetivos.",
        "Não permita que ninguém defina seus limites.",
        "As dificuldades o preparam para o sucesso.",
        "Acredite em seu potencial e comece a agir.",
        "Pense positivo e coisas positivas acontecerão.",
        "A felicidade não é algo pronto. Ela vem das suas próprias ações.",
        "O sucesso é construído sobre falhas.",
        "O fracasso não é a derrota, é uma oportunidade para recomeçar.",
        "Sempre que você encontrar um obstáculo, lembre-se: este é apenas um momento.",
        "Mantenha a calma e continue seguindo em frente.",
        "O sucesso não é para os preguiçosos.",
        "Tudo começa com um sonho.",
        "Você pode mudar o mundo se mudar a si mesmo.",
        "Acredite em seus sonhos e trabalhe duro para alcançá-los.",
        "Não permita que seus medos o impeçam de alcançar seus objetivos.",
        "O sucesso é a soma de pequenos esforços repetidos diariamente.",
        "O sucesso não é o resultado de um único evento, é o resultado de anos de trabalho duro.",
        "Não desista. Seu sucesso está mais perto do que você imagina.",
        "Sempre mantenha sua mente focada em seus objetivos.",
        "O sucesso não é uma questão de sorte, é uma questão de trabalho árduo e dedicação.",
        "As dificuldades são oportunidades para crescer e melhorar.",
        "O sucesso não é uma viagem fácil, mas é uma viagem que vale a pena.",
        "Não desista quando as coisas ficarem difíceis. Esse é o momento de se esforçar ainda mais.",
        "O sucesso é a soma de todos os seus esforços.",
        "Não deixe o medo impedir você de realizar seus sonhos.",
        "Acredite em si mesmo e tudo será possível.",
        "Grandes coisas nunca vêm de zonas de conforto.",
        "Não há atalhos para o sucesso.",
        "Acredite em seus sonhos e eles podem se tornar realidade.",
        "O fracasso é apenas uma oportunidade para aprender e crescer.",
        "Não desista até estar orgulhoso.",
        "O sucesso é a soma de pequenas coisas feitas de forma consistente.",
        "Não deixe o medo de falhar impedi-lo de tentar.",
        "O sucesso é construído sobre a base do fracasso.",
        "O sucesso não é para os sortudos, é para os trabalhadores árduos.",
        "Você é capaz de conquistar tudo que deseja.",
        "Acredite em si mesmo e você irá longe.",
        "Não desista, continue tentando.",
        "O sucesso é uma jornada, não um destino.",
        "A cada dia você está mais perto de alcançar seus objetivos.",
        "Com determinação e trabalho duro, você pode superar qualquer obstáculo.",
        "Acredite no seu potencial e tudo é possível.",
        "A sua determinação é a chave para o sucesso.",
        "Se você quer algo, vá atrás e conquiste.",
        "Nunca subestime a sua capacidade de superar desafios.",
        "Acredite no poder dos seus sonhos.",
        "A sua persistência irá levá-lo aonde você quer chegar.",
        "Com trabalho duro, tudo é possível.",
        "Nunca desista de seus sonhos.",
        "Você é mais forte do que imagina.",
        "O sucesso é o resultado de trabalho duro e dedicação.",
        "Não importa quantas vezes você caia, o importante é sempre se levantar.",
        "Acredite em si mesmo e nada será impossível.",
        "Acredite na sua capacidade de vencer.",
        "O sucesso é uma questão de perseverança.",
        "Acredite em você e faça acontecer.",
        "Seja persistente e nunca desista de seus objetivos.",
        "O sucesso é uma questão de atitude.",
        "Nunca deixe de acreditar em seus sonhos.",
        "O sucesso é resultado de perseverança e determinação.",
        "Se você quer algo, vá atrás e conquiste.",
        "Com trabalho duro e dedicação, tudo é possível.",
        "Nunca deixe de acreditar em si mesmo.",
        "Você é capaz de realizar seus sonhos.",
        "Acredite em sua capacidade de superar desafios.",
        "O sucesso é uma questão de tempo e esforço.",
        "Com determinação e dedicação, tudo é possível.",
        "Acredite em você e siga em frente.",
        "Não desista de seus sonhos, você pode realizá-los.",
        "O sucesso é o resultado de trabalho duro e perseverança.",
        "Com foco e determinação, você pode conquistar o mundo.",
        "Não deixe que o medo impeça você de seguir em frente.",
        "Acredite em seu potencial e faça acontecer.",
        "O sucesso é uma questão de escolhas e atitudes.",
        "Não deixe que o fracasso o impeça de seguir em frente.",
        "Acredite em você e vá em frente.",
        "O sucesso é resultado de trabalho árduo e dedicação.",
        "Acredite em sua capacidade de superar desafios e tudo é possível.",
        "Não deixe que os obstáculos o impeçam de alcançar seus objetivos.",
        "Acredite em você mesmo e faça acontecer.",
        "Com determinação e perseverança, nada é impossível.",
        "Nunca desista de seus sonhos, você pode realizá-los.",
        "Acredite em sua capacidade de superar desafios e siga em frente.",
        "O sucesso é resultado de trabalho duro, foco e determinação.",
        "O sucesso é a soma de pequenos esforços repetidos dia após dia."
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cookieImageView = findViewById(R.id.cookieImageView)
        messageTextView = findViewById(R.id.messageTextView)
    }

    fun openCookie(view: View) {
        cookieImageView.setImageResource(R.drawable.cookies_open)
        val random = Random()
        val message = messages[random.nextInt(messages.size)]
        messageTextView.text = message
    }
}
